import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyContactCardComponent } from './my-contact-card/my-contact-card.component';
import { MyFirstModuleModule } from './my-first-module/my-first-module.module';

@NgModule({
  declarations: [
    AppComponent,
    MyContactCardComponent
  ],
  imports: [
    BrowserModule,
    MyFirstModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
